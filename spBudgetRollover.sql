﻿------------------------------------------------------------------------------------------------------------------------
-- 
-- Procedure       : dbo.SpBudgetRollover
-- 
-- File Name       : SpBudgetRollover.sql
-- 
-- System Name     : Code Commenting Training
-- 
-- Author          : A. Programmer (Bluesmith)
-- 
-- Description     : Creates budget data for the specified year by copying the budget data from the previous year
-- 
-- Parameters      : @newYear - The year to create budgets for.
--
-- Version History
-- 
-- Version Date       Who   Description
-- ------- ---------- ----- -----------
-- 1.0     01/01/2014 AP    Original version.
--
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE dbo.SpBudgetRollover
-- Parameter -Year 
    @NewYear VARCHAR(2) = NULL
AS

BEGIN

-- Calculate current financial year as 4 digits 
-- Work out the last financial year.
-- For each existing budget check it does not already exist.
-- If it does not exists create a budget but only if in use last year.
-- If it does not exist create an owner but only if the owner does not.
-- For each budget group check if it already exists.
-- If it does not create a budget group.

DECLARE @GC_B_FAIL BIT
SET @GC_B_FAIL = 1

DECLARE @i INTEGER; /* integer count i */
DECLARE @rowCount INTEGER;
DECLARE @oldYear VARCHAR(2);
DECLARE @temp VARCHAR(64);
DECLARE @ownerCode VARCHAR(4); /* owner code */
DECLARE @ownerName VARCHAR(32); 
DECLARE @groupCode VARCHAR(4); /* group code */
DECLARE @budgetCode VARCHAR(4); /* budget code */
DECLARE @alertLevel DECIMAL(10, 2); /* alert level     */
DECLARE @prohibitLevel DECIMAL(10, 2); /* prohibit level                                */
DECLARE @name VARCHAR(32); /* budget owner name                             */
DECLARE @inuse VARCHAR(1); /* in use                                        */
DECLARE @strTraceMessage VARCHAR(2000)

BEGIN TRY

    SET @temp = RTRIM(LTRIM(@NewYear)); -- Get rid of leading and trailing space 

                                                  /*----------------------------------------------*/
                                                  /* Can't figure out what rest of this is for as */
                                                  /* @temp not used ??                            */
                                                  /*----------------------------------------------*/
    WHILE (LEN(@temp) < 2) 
    BEGIN
            SET @temp = '0' + @temp;
    END;
    IF (@temp > '50') 
    BEGIN
      SET @temp = '19' + 
        @temp;
    END
    ELSE
    BEGIN
        SET @temp = '20' 
            + @temp;
    END;
                          /*-------------------------*/
                          /* convert year to integer */
                          /*-------------------------*/
    SET @i = CONVERT(INT, @NewYear);
                          /*--------------------------*/
                          /* subtract 1 from the year */
                          /*--------------------------*/
    SET @i = @i - 1;
                          /*----------------------------------------------*/
                          /* If i < 0 then must have been year 2000 say   */
                          /* set to 99.                                   */
                          /*----------------------------------------------*/
    IF (@i < 0) 
    BEGIN
        SET @i = 99;
    END;
    SET @oldYear = RTRIM(LTRIM(@i));
    WHILE (LEN(@oldYear) < 2) BEGIN SET @oldyear = '0' + @oldYear; END;

                                                  /*----------------------------------------------*/
                                                  /* Main processing starts here                  */
                                                  /*----------------------------------------------*/

    BEGIN TRANSACTION;

                                                  /*----------------------------------------------*/
                                                  /* NOTE - This is poor customer SQL which could */
                                                  /* have been done a lot better!                 */
                                                  /*----------------------------------------------*/

    DECLARE cur1 CURSOR FOR
    SELECT Budget_Code, Alert_Level, 
      Prohibit_Level, Name FROM 
    Budget WHERE   
    Year = @oldYear AND Inuse <> 'N';
    OPEN cur1;

    FETCH NEXT FROM cur1 INTO
    @budgetCode, @alertLevel, @prohibitLevel, @name
    WHILE @@FETCH_STATUS = 0
    BEGIN  SELECT @inuse = Inuse FROM Budget 
      WHERE  
        Budget_Code = @budgetCode AND Year        = @NewYear;

                                                  /*==============================================*/
                                                  /* Log a message                                */
                                                  /*==============================================*/
        SET @rowCount = @@ROWCOUNT;
        SELECT @strTraceMessage = CONVERT(VARCHAR, @rowCount)
        EXEC SpDebugTrace 'Budgets', @strTraceMessage, 'SpBudgetRollover'
        
    IF (@rowCount = 0) 
    BEGIN
        INSERT INTO Budget (Budget_Code, Year, Name, Creation_Date, Alert_Level, 
        Prohibit_Level, Inuse)
        VALUES (@budgetCode, @NewYear, @name, GETDATE(), @alertLevel, 
        @prohibitLevel, 'Y')

                                              /*==============================================*/
                                              /* Log a message                                */
                                              /*==============================================*/
        EXEC SpDebugTrace 'Budgets', 'Created budget', 
        'SpBudgetRollover'
        END
      ELSE IF (@inuse = 'N') 
    BEGIN
        UPDATE Budget	
        SET     Name            = @name, Creation_Date   = GETDATE(),
            Alert_Level     = @alertLevel, Prohibit_Level  = @prohibitLevel,
            Inuse           = 'Y'
        WHERE   Budget_Code = @budgetCode AND Year        = @NewYear;
        
                                              /*==============================================*/
                                              /* Log a message                                */
                                              /*==============================================*/
        EXEC SpDebugTrace 'Budgets', 'Updated budget', 
        'SpBudgetRollover'
    END;

    DECLARE cur2 CURSOR FOR
    SELECT Owner_Code, Name FROM 
      Budget_Owner WHERE   
    Budget_Code = @budgetCode AND Year = @oldYear;
    OPEN cur2;

    FETCH NEXT FROM cur2 INTO
    @ownerCode, @ownerName
    WHILE @@FETCH_STATUS = 0
    BEGIN  
    
    SELECT 1 FROM Budget_Owner 
     WHERE Budget_Code = @budgetCode
       AND Year = @NewYear 
    AND Owner_Code  = @ownerCode
    
                                          /*------------------------------------*/
                                          /* Log a message                      */
                                          /*------------------------------------*/
    SET @rowCount = @@ROWCOUNT;
    SELECT @strTraceMessage = CONVERT(VARCHAR, @rowCount)
    EXEC SpDebugTrace 'Owners', @strTraceMessage, 'SpBudgetRollover'
    
    IF (@rowCount = 0) 
    BEGIN
        INSERT INTO Budget_Owner (Budget_Code, Year, Owner_Code, Name) VALUES 
           (@budgetCode, @NewYear, @ownerCode, @ownerName);

                                          /*------------------------------------*/
                                          /* Log a message                      */
                                          /*------------------------------------*/
        EXEC SpDebugTrace 'Owners', 'Created owner', 
        'SpBudgetRollover'
    END;
        FETCH NEXT FROM cur2 INTO
        @ownerCode, @ownerName
    END; 
        CLOSE cur2;
        DEALLOCATE cur2;

    FETCH NEXT FROM cur1 INTO
    @budgetCode, @alertLevel, @prohibitLevel, @name
    END;
    CLOSE cur1;
    DEALLOCATE cur1;

    -- Create budget groups for the current financial year from those for last year. 

    DECLARE cur3 CURSOR FOR
    SELECT Group_Code, Budget_Code, Owner_Code FROM 
      Budget_Group WHERE   
    Year = @oldYear;
    OPEN cur3;

    FETCH NEXT FROM cur3 INTO
    @groupCode, @budgetCode, @ownerCode
    WHILE @@FETCH_STATUS = 0
    BEGIN  
                                                  /*----------------------------------------------*/
                                                  /* Create the budget group                      */
                                                  /*----------------------------------------------*/

        SELECT 1 FROM  Budget_Group 
        WHERE Group_Code  = @groupCode AND Budget_Code = @budgetCode
        AND Owner_Code  = @ownerCode AND Year        = @NewYear;

                                              /*------------------------------------*/
                                              /* Log a message                      */
                                              /*------------------------------------*/
    SET @rowCount = @@ROWCOUNT;
    SELECT @strTraceMessage = CONVERT(VARCHAR, @rowCount)
    EXEC SpDebugTrace 'Owners', @strTraceMessage, 'SpBudgetRollover'
    
    IF (@rowCount = 0) BEGIN
    INSERT INTO Budget_Group
           (Group_Code, Year, Budget_Code, Owner_Code) VALUES 
           (@groupCode, @NewYear, @budgetCode, @ownerCode);

                                              /*----------------------------------------------*/
                                              /* Log a message                                */
                                              /*----------------------------------------------*/
    EXEC SpDebugTrace 'Owners', 'Created owner', 'SpBudgetRollover'

    END;
    FETCH NEXT FROM cur3 INTO
    @groupCode, @budgetCode, @ownerCode
    END;
    CLOSE cur3;
    DEALLOCATE cur3;
    
    COMMIT; 

END TRY
BEGIN CATCH
    SELECT @strTraceMessage = 'Failed with error - ' + ERROR_MESSAGE()
        EXEC SpDebugTrace 'ERROR', @strTraceMessage, 
          'SpBudgetRollover'
    ROLLBACK;
    RETURN @GC_B_FAIL
END CATCH

END

------------------------------------------------------------------------------------------------------------------------
--                                                  End of procedure 
------------------------------------------------------------------------------------------------------------------------
